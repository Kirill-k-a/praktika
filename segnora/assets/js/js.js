function bg1 (i) 
{
    var imgUrl = $('.carousel-inner').contents().contents("img:eq("+i+")").attr("src");
    $('#bg').css("background-image",
    "url(" + imgUrl + ")");
};
function backgroundload(){
    $('.card-img-top').each(function(){
        s=$(this).attr('src');
        c=$(this).parent().parent().parent();
        c=c.children('.bg-card');
        c.css("background-image",
        "url(" + s + ")");
    });
}

window.onload=function()
{

    $('#result').on('DOMSubtreeModified', backgroundload);
    backgroundload();
    //bg();
    bg1(0);
    $('#carouselExampleIndicators').on('slide.bs.carousel', function (e) {
        bg1(e.to);
    });
    //scroll
    $("#carusel2-next").click(function(){
        s=$("#scroll2").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll2").scrollLeft(s+w);
    });
    $("#carusel2-prev").click(function(){
        s=$("#scroll2").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll2").scrollLeft(s-w);
    });
    $("#carusel3-next").click(function(){
        s=$("#scroll3").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll3").scrollLeft(s+w);
    });
    $("#carusel3-prev").click(function(){
        s=$("#scroll3").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll3").scrollLeft(s-w);
    });
    $("#carusel4-next").click(function(){
        s=$("#scroll4").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll4").scrollLeft(s+w);
    });
    $("#carusel4-prev").click(function(){
        s=$("#scroll4").scrollLeft();
        w=$("#scroll2").contents("div").contents(".card-deck").outerWidth()+30;
        $("#scroll4").scrollLeft(s-w);
    });

        // Цепляем обработчик на все нужные элементы
    $('.radio').change(function() {
    // У всех элементов, кроме кликнутого, снимаем пометку
    $('.radio').not(this).prop({checked: false});
    });
    
    $('#myDropDown').val(''); 
    $('#myDropDown1').val(''); 

    $('input[type=radio][data-toggle=radio-collapse]').each(function(index, item) {
        var $item = $(item);
        var $target = $($item.data('target'));
        $('input[type=radio][name="' + item.name + '"]').on('change', function() {
            if($item.is(':checked')){
                $target.collapse('show');
            }
            else {
                $target.collapse('hide');}
        });
    });
    // $("#button_form").on('click',function(t) {
    //     $("#button_form").prop('disabled', true);
    // });
    $(document).ajaxStart(function() {
        $(".button_form").prop('disabled', true);
     });//.ajaxStop(function() {
    //     $("#button_form").prop('disabled', false);
    //  });

   
};

