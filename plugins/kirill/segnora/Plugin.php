<?php namespace Kirill\Segnora;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            '\Kirill\Segnora\components\basket_component' => 'basket_component',
        ];
    }

    public function registerSettings()
    {
    }

}
