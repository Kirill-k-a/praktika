<?php namespace Kirill\Segnora\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Card extends Controller
{
    
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
        ];

        public $relationConfig = 'config_relation.yaml';

    // public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Kirill.Segnora', 'main-menu-item');
    }
}
