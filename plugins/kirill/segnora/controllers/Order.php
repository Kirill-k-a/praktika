<?php namespace Kirill\Segnora\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Support\Facades\Input;
use Request;

class Order extends Controller
{
    // public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    // ~/plugins/kirill/segnora/controllers/order/_order_tovar.htm
    // public $listConfig = 'config_list.yaml';
    // public $formConfig = 'config_form.yaml';
    // public $reorderConfig = 'config_reorder.yaml';
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
        ];

        public $relationConfig = 'config_relation.yaml';

    // public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Kirill.Segnora', 'main-menu-item', 'side-menu-item6');
    }
    public function onDeleteFromOrder2()
    { 
        $del_order = Input::get('checked');
        foreach($del_order as $a)
        {
            $order = \kirill\segnora\Models\order::where('id', '=', $a);
            $order_tovar= $order->get()[0]->order_tovar;
            foreach($order_tovar as $b)
            {
                $t = \kirill\segnora\Models\size::where('card_id', '=', $b->card->id)
                ->where('size', '=', $b->size)->get()[0];
                $t->quantity += 1;
                $t->save();
                $b->delete();
            }
            $order->delete();
        }
        return $this->listRefresh();
    }

    public function onDeleteFromOrder_tovar2()
    { 
        $del_order = Input::get('checked');
        foreach($del_order as $a)
        {
            $order_tovar = \kirill\segnora\Models\order_tovar::where('id', '=', $a);
            $tovar = $order_tovar->get();
            if(sizeof($tovar)){
                $quantity = \kirill\segnora\Models\size::where('card_id', '=', $tovar[0]->id_card)
                ->where('size', '=', $tovar[0]->size)->get();
                if (sizeof($quantity)){
                    $quantity[0]->quantity += 1;
                    $quantity[0]->save();
                    $order_tovar->delete();
                }
            }
        }
        // return $this->relationRefresh('basket_tovar');
        // October BUGs - init again
        $model_id = post("model_id");
        $model = \kirill\segnora\Models\Order::find($model_id);
        $this->initForm($model);
        $this->initRelation($model,"order_tovar");
        return $this->relationRefresh('order_tovar');
    }
}
