<?php namespace Kirill\Segnora\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Illuminate\Support\Facades\Input;
use Request;

class Basket extends Controller
{
    // public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    // public $listConfig = 'config_list.yaml';
    // public $formConfig = 'config_form.yaml';
    // public $reorderConfig = 'config_reorder.yaml';
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
        ];

        public $relationConfig = 'config_relation.yaml';

    // public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Kirill.Segnora', 'main-menu-item', 'side-menu-item7');
    }

    public function onDeleteFromCart2(){ 

        $del_cart = Input::get('checked');
        foreach($del_cart as $a)
        {
            $basket = \kirill\segnora\Models\basket::where('id', '=', $a);
            
            $basket_tovar= $basket->get()[0]->basket_tovar;

            foreach($basket_tovar as $b)
            {
                $t = \kirill\segnora\Models\size::where('card_id', '=', $b->card->id)
                ->where('size', '=', $b->size)->get()[0];
                $t->quantity += 1;
                $t->save();

                $b->delete();
            }

            $basket->delete();
        }
        return $this->listRefresh();
    }


    public function onDeleteFromCart_tovar2(){ 
        $del_cart = Input::get('checked');
        foreach($del_cart as $a)
        {
            $basket_tovar = \kirill\segnora\Models\basket_tovar::where('id', '=', $a);
            $tovar = $basket_tovar->get();
            if(sizeof($tovar)){
                $quantity = \kirill\segnora\Models\size::where('card_id', '=', $tovar[0]->id_card)
                ->where('size', '=', $tovar[0]->size)->get();
                if (sizeof($quantity)){
                    $quantity[0]->quantity += 1;
                    $quantity[0]->save();
                    $basket_tovar->delete();
                }
            }
        }
        // October BUGs - init again
        $model_id = post("model_id");
        $model = \kirill\segnora\Models\Basket::find($model_id);
        $this->initForm($model);
        $this->initRelation($model,"basket_tovar");
        return $this->relationRefresh('basket_tovar');
    }

    // public function relationMakePartial($partial, $params = [])
    // {
    //     $inbuiltPartials = ['add', 'create', 'update', 'delete', 'remove', 'link', 'unlink'];

    //     if (substr($partial, 0, strlen('button_')) == 'button_') {
    //         $action = substr($partial, strlen('button_'));
    //         if(!in_array($action, $inbuiltPartials)) {
    //             $contents = $this->makePartial($partial, $params + $this->vars, false);
    //             return $contents;
    //         }
    //     }

    //     return parent::relationMakePartial($partial, $params);
    // }
    
}
