<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrderTovar3 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->integer('order_id');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->dropColumn('order_id');
        });
    }
}
