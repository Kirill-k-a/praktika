<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCountry extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_country', function($table)
        {
            $table->string('name_country');
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('name');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_country', function($table)
        {
            $table->dropColumn('name_country');
            $table->increments('id')->unsigned()->change();
            $table->string('name', 191);
        });
    }
}
