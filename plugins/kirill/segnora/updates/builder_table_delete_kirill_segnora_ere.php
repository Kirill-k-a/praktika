<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraEre extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_ere');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_ere', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigInteger('d');
        });
    }
}
