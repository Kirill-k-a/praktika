<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCart extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_cart', function($table)
        {
            $table->integer('id_user')->nullable()->change();
            $table->integer('id_card')->nullable()->change();
            $table->integer('quantity')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_cart', function($table)
        {
            $table->integer('id_user')->nullable(false)->change();
            $table->integer('id_card')->nullable(false)->change();
            $table->integer('quantity')->nullable(false)->change();
        });
    }
}
