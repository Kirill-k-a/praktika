<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraSize extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_size');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_size', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('size', 191);
            $table->string('quantity', 191);
        });
    }
}
