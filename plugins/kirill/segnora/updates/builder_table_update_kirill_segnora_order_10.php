<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder10 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('city')->nullable();
            $table->string('adress_magazin')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('adress', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->dropColumn('city');
            $table->dropColumn('adress_magazin');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->string('adress', 191)->nullable(false)->change();
        });
    }
}
