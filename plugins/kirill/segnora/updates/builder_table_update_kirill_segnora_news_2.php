<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraNews2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_news', function($table)
        {
            $table->string('photo');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_news', function($table)
        {
            $table->dropColumn('photo');
        });
    }
}
