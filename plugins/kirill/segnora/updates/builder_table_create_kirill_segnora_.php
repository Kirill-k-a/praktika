<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnora extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id_user');
            $table->string('name_user');
            $table->string('pasword');
            $table->string('mail');
            $table->string('phone');
            $table->dateTime('data');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_');
    }
}
