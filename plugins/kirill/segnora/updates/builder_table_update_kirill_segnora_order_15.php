<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder15 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->integer('id_card')->nullable(false)->change();
            $table->integer('size')->nullable(false)->change();
            $table->string('email', 191)->nullable(false)->change();
            $table->string('name', 191)->nullable(false)->change();
            $table->string('surname', 191)->nullable(false)->change();
            $table->string('adress', 191)->nullable(false)->change();
            $table->string('phone', 15)->nullable(false)->change();
            $table->string('city', 191)->nullable(false)->change();
            $table->string('adress_magazin', 191)->nullable(false)->change();
            $table->dropColumn('data');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->integer('id_card')->nullable()->change();
            $table->integer('size')->nullable()->change();
            $table->string('email', 191)->nullable()->change();
            $table->string('name', 191)->nullable()->change();
            $table->string('surname', 191)->nullable()->change();
            $table->string('adress', 191)->nullable()->change();
            $table->string('phone', 15)->nullable()->change();
            $table->string('city', 191)->nullable()->change();
            $table->string('adress_magazin', 191)->nullable()->change();
            $table->dateTime('data')->nullable();
        });
    }
}
