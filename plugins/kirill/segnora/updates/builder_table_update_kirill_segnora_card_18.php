<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard18 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->dropColumn('size');
            $table->string('id_size', 10)->default('0')->create();;
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->dropColumn('id_size');
            $table->string('size', 10)->default(null)->create();
        });
    }
}
