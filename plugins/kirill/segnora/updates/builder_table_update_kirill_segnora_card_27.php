<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard27 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->integer('id_size')->default(0)->change();
            $table->integer('id_clothing_type')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->integer('id_size')->default(null)->change();
            $table->integer('id_clothing_type')->default(null)->change();
        });
    }
}
