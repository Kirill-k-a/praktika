<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder3 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->dropPrimary(['id']);
        });
    }
}
