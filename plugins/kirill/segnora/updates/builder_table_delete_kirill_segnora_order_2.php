<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraOrder2 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_order');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_card');
            $table->integer('id_user');
            $table->integer('size');
            $table->boolean('took_the_goods')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('email', 191);
            $table->string('name', 191);
            $table->string('surname', 191);
            $table->string('adress', 191);
            $table->integer('phone');
        });
    }
}
