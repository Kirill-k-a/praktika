<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCabinet extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_', 'kirill_segnora_cabinet');
        Schema::table('kirill_segnora_cabinet', function($table)
        {
            $table->increments('id_user')->unsigned(false)->change();
            $table->string('name_user')->change();
            $table->string('pasword')->change();
            $table->string('mail')->change();
            $table->string('phone')->change();
        });
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_cabinet', 'kirill_segnora_');
        Schema::table('kirill_segnora_', function($table)
        {
            $table->increments('id_user')->unsigned()->change();
            $table->string('name_user', 191)->change();
            $table->string('pasword', 191)->change();
            $table->string('mail', 191)->change();
            $table->string('phone', 191)->change();
        });
    }
}
