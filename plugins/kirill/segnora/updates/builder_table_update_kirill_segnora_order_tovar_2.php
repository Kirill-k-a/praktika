<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrderTovar2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('total_price');
            $table->dropColumn('order_number');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('total_price', 191)->nullable();
            $table->string('order_number', 191);
        });
    }
}
