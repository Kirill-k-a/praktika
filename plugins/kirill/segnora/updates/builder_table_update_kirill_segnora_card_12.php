<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard12 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->dropColumn('slug');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->string('slug', 191)->nullable();
        });
    }
}
