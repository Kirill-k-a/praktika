<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard22 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->integer('discount')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->integer('discount')->default(null)->change();
        });
    }
}
