<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraOrder3 extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_card');
            $table->integer('id_user');
            $table->integer('size');
            $table->boolean('took_the_goods')->default(0);
            $table->string('email');
            $table->string('name');
            $table->string('surname');
            $table->string('adress');
            $table->integer('phone');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_order');
    }
}
