<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraCard extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_card', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('composition');
            $table->string('country');
            $table->string('price');
            $table->string('photo');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_card');
    }
}
