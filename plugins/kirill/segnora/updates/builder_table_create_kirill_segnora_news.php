<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraNews extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('news_text');
            $table->date('data');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_news');
    }
}
