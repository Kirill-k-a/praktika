<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket9 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropPrimary(['id_basket']);
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->primary(['id_basket']);
        });
    }
}
