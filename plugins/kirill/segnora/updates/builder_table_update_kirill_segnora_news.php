<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraNews extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_news', function($table)
        {
            $table->string('news_text', 2000)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_news', function($table)
        {
            $table->string('news_text', 191)->change();
        });
    }
}
