<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSizeQuantity extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_size', 'kirill_segnora_size_quantity');
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_size_quantity', 'kirill_segnora_size');
    }
}
