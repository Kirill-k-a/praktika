<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket6 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->integer('id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->integer('id')->default(null)->change();
        });
    }
}
