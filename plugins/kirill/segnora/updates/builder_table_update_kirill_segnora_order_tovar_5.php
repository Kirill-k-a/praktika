<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrderTovar5 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->dropColumn('email');
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('adress');
            $table->dropColumn('phone');
            $table->dropColumn('city');
            $table->dropColumn('adress_magazin');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order_tovar', function($table)
        {
            $table->string('email', 191);
            $table->string('name', 191);
            $table->string('surname', 191);
            $table->string('adress', 191)->nullable();
            $table->string('phone', 15);
            $table->string('city', 191)->nullable();
            $table->string('adress_magazin', 191)->nullable();
        });
    }
}
