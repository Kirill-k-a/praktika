<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->integer('id_user');
            $table->dropColumn('price');
            $table->dropColumn('photo');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropColumn('id_user');
            $table->integer('price');
            $table->integer('photo');
        });
    }
}
