<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('email');
            $table->string('name');
            $table->string('surname');
            $table->string('adress');
            $table->integer('phone');
            $table->boolean('took_the_goods')->nullable(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->dropColumn('email');
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('adress');
            $table->dropColumn('phone');
            $table->boolean('took_the_goods')->nullable()->change();
        });
    }
}
