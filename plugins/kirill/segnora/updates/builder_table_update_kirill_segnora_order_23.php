<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder23 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('email');
            $table->string('name');
            $table->string('surname');
            $table->string('adress')->nullable();
            $table->string('phone');
            $table->string('city')->nullable();
            $table->string('adress_magazin')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->dropColumn('email');
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('adress');
            $table->dropColumn('phone');
            $table->dropColumn('city');
            $table->dropColumn('adress_magazin');
        });
    }
}
