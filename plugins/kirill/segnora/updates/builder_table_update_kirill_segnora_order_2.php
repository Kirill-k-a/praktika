<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('email')->change();
            $table->string('name')->change();
            $table->string('surname')->change();
            $table->string('adress')->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('email', 191)->change();
            $table->string('name', 191)->change();
            $table->string('surname', 191)->change();
            $table->string('adress', 191)->change();
        });
    }
}
