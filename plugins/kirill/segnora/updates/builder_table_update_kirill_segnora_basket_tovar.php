<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasketTovar extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_basket', 'kirill_segnora_basket_tovar');
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->renameColumn('id_user', 'id_basker');
            $table->dropColumn('id');
        });
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_basket_tovar', 'kirill_segnora_basket');
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->renameColumn('id_basker', 'id_user');
            $table->increments('id');
        });
    }
}
