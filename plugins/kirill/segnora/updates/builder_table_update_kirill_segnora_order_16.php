<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder16 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('adress', 191)->nullable()->change();
            $table->string('city', 191)->nullable()->change();
            $table->string('adress_magazin', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->string('adress', 191)->nullable(false)->change();
            $table->string('city', 191)->nullable(false)->change();
            $table->string('adress_magazin', 191)->nullable(false)->change();
        });
    }
}
