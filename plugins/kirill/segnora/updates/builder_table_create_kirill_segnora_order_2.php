<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraOrder2 extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('id_card');
            $table->integer('id_user');
            $table->integer('size');
            $table->boolean('took_the_goods')->nullable()->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_order');
    }
}
