<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraSize extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_size', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('size');
            $table->string('availability');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_size');
    }
}
