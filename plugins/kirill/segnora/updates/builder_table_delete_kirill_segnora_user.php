<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraUser extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_user');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_user', 191);
            $table->string('pasword', 191);
            $table->string('mail', 191);
            $table->string('phone', 191);
        });
    }
}
