<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraClothingType extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_clothing_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name_clothing');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_clothing_type');
    }
}
