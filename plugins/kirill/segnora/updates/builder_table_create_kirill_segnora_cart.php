<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraCart extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_cart', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_card');
            $table->integer('quantity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_cart');
    }
}
