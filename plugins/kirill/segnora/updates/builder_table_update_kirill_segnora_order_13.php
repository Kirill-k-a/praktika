<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrder13 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->integer('id_card')->nullable()->change();
            $table->integer('size')->nullable()->change();
            $table->boolean('took_the_goods')->nullable()->change();
            $table->string('email', 191)->nullable()->change();
            $table->string('name', 191)->nullable()->change();
            $table->string('surname', 191)->nullable()->change();
            $table->string('phone', 15)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_order', function($table)
        {
            $table->integer('id_card')->nullable(false)->change();
            $table->integer('size')->nullable(false)->change();
            $table->boolean('took_the_goods')->nullable(false)->change();
            $table->string('email', 191)->nullable(false)->change();
            $table->string('name', 191)->nullable(false)->change();
            $table->string('surname', 191)->nullable(false)->change();
            $table->string('phone', 15)->nullable(false)->change();
        });
    }
}
