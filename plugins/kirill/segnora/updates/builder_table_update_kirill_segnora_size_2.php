<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSize2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->renameColumn('id_size', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->renameColumn('id', 'id_size');
        });
    }
}
