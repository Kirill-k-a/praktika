<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket7 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropPrimary(['id']);
            $table->renameColumn('id', 'id_basket');
            $table->primary(['id_basket']);
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropPrimary(['id_basket']);
            $table->renameColumn('id_basket', 'id');
            $table->primary(['id']);
        });
    }
}
