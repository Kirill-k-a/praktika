<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket11 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dateTime('data')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropColumn('data');
        });
    }
}
