<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraUser3 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_user', function($table)
        {
            $table->renameColumn('id_user', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_user', function($table)
        {
            $table->renameColumn('id', 'id_user');
        });
    }
}
