<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraUser extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_cabinet', 'kirill_segnora_user');
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_user', 'kirill_segnora_cabinet');
    }
}
