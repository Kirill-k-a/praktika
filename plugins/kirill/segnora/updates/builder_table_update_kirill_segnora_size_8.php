<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSize8 extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_size_quantity', 'kirill_segnora_size');
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_size', 'kirill_segnora_size_quantity');
    }
}
