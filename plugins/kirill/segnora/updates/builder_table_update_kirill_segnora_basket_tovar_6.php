<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasketTovar6 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->dropPrimary(['id_card']);
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->primary(['id_card']);
        });
    }
}
