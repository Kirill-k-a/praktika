<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraOrder4 extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('order_number');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_order');
    }
}
