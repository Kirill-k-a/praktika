<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraOrderTovar extends Migration
{
    public function up()
    {
        Schema::rename('kirill_segnora_order', 'kirill_segnora_order_tovar');
    }
    
    public function down()
    {
        Schema::rename('kirill_segnora_order_tovar', 'kirill_segnora_order');
    }
}
