<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraEre extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_ere', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigInteger('d');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_ere');
    }
}
