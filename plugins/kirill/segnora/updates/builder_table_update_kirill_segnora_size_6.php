<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSize6 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->renameColumn('id_card', 'card_id');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->renameColumn('card_id', 'id_card');
        });
    }
}
