<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard20 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->renameColumn('country', 'id_country');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->renameColumn('id_country', 'country');
        });
    }
}
