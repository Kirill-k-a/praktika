<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasketTovar2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->renameColumn('id_basker', 'id_basket');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->renameColumn('id_basket', 'id_basker');
        });
    }
}
