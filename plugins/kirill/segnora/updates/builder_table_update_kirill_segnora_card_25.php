<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard25 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->integer('id_size')->nullable()->unsigned(false)->default(0)->change();
            $table->integer('id_clothing_type')->nullable(false)->unsigned(false)->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->string('id_size', 10)->nullable()->unsigned(false)->default('0')->change();
            $table->string('id_clothing_type', 191)->nullable(false)->unsigned(false)->default('0')->change();
        });
    }
}
