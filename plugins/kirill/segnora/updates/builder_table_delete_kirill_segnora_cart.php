<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraCart extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_cart');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_cart', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('id_user')->nullable();
            $table->integer('id_card')->nullable();
            $table->integer('quantity')->nullable();
        });
    }
}
