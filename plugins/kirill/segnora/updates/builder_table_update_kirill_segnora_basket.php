<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasket extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->integer('price');
            $table->integer('photo');
            $table->integer('size');
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('id_user');
            $table->dropColumn('id_size');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket', function($table)
        {
            $table->dropColumn('price');
            $table->dropColumn('photo');
            $table->dropColumn('size');
            $table->increments('id')->unsigned()->change();
            $table->integer('id_user');
            $table->integer('id_size');
        });
    }
}
