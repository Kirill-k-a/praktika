<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraUser extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('login');
            $table->string('password');
            $table->string('last_name');
            $table->string('first_name');
            $table->integer(' phone');
            $table->string('email');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_user');
    }
}
