<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSize7 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->integer('card_id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->integer('card_id')->default(null)->change();
        });
    }
}
