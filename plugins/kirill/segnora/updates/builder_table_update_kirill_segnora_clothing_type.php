<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraClothingType extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_clothing_type', function($table)
        {
            $table->renameColumn('name_clothing', 'type_clothing');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_clothing_type', function($table)
        {
            $table->renameColumn('type_clothing', 'name_clothing');
        });
    }
}
