<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraCard24 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->string('id_size', 10)->nullable()->change();
            $table->integer('discount')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_card', function($table)
        {
            $table->string('id_size', 10)->nullable(false)->change();
            $table->integer('discount')->nullable(false)->change();
        });
    }
}
