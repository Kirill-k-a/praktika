<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraOrder extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_order');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_order', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('id_card');
            $table->integer('id_user');
            $table->integer(' quantity');
            $table->integer(' order_price');
            $table->dateTime('data_order');
        });
    }
}
