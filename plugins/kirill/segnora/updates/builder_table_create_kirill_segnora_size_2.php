<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKirillSegnoraSize2 extends Migration
{
    public function up()
    {
        Schema::create('kirill_segnora_size', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id_card');
            $table->integer('quantity');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('kirill_segnora_size');
    }
}
