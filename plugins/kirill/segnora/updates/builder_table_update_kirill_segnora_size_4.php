<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraSize4 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->integer('size');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_size', function($table)
        {
            $table->dropColumn('size');
        });
    }
}
