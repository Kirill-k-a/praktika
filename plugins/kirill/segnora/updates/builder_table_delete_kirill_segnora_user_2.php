<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteKirillSegnoraUser2 extends Migration
{
    public function up()
    {
        Schema::dropIfExists('kirill_segnora_user');
    }
    
    public function down()
    {
        Schema::create('kirill_segnora_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('login', 191);
            $table->string('password', 191);
            $table->string('last_name', 191);
            $table->string('first_name', 191);
            $table->integer(' phone');
            $table->string('email', 191);
        });
    }
}
