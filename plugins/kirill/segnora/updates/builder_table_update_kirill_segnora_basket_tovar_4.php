<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraBasketTovar4 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->integer('id_card')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_basket_tovar', function($table)
        {
            $table->integer('id_card')->default(null)->change();
        });
    }
}
