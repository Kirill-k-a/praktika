<?php namespace Kirill\Segnora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateKirillSegnoraUser2 extends Migration
{
    public function up()
    {
        Schema::table('kirill_segnora_user', function($table)
        {
            $table->dropColumn('data');
        });
    }
    
    public function down()
    {
        Schema::table('kirill_segnora_user', function($table)
        {
            $table->dateTime('data');
        });
    }
}
