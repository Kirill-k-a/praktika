<?php namespace Kirill\Segnora\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Auth;
use Request;


class basket_component extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name'        => 'basket_component',
            'description' => 'Корзина заказа'
        ];
    }

    public function onDeleteCart(){ 
      $tovars = Db::table('kirill_segnora_basket');
      foreach($tovars as $i)
      {
        Db::table('kirill_segnora_basket')->where('id_user', '=', $user = Auth::getUser()->id)->delete();
      }
      return [
        '.bsk' => $this->renderPartial('bsk.htm'),
      ];
    }

    public function onDeleteOrder(){ 

      $del_order = Input::get('del_order');
      
      $order = Db::table('kirill_segnora_order');
      foreach($order as $ord)
      {
        
        Db::table('kirill_segnora_order')->where('order_number', '=', $del_order)->delete();

      }
      return [
        '.bsk' => $this->renderPartial('history.htm'),
      ];
    }


    //ПЕРЕДЕЛАЛ


    public function onDeleteFromCart(){ 

      // $del_cart = Input::get('del_cart');
      $id_basket_tovar = Input::get('id');
      $d = Db::table('kirill_segnora_basket_tovar')->where('id', '=', $id_basket_tovar);
      $dd=$d->get()[0];

      $t = \kirill\segnora\Models\size::where('card_id', '=', $dd->id_card)
      ->where('size', '=', $dd->size)->get()[0];
      $t->quantity += 1;
      $t->save();

      $d->delete();
      
      return [
        '.bsk' => $this->renderPartial('bsk.htm', ['user'=>Auth::getUser()]),
      ];
    }

        //ПЕРЕДЕЛАЛ

    public function onAddToCart(){

        $user = Auth::getUser();

        $tovar = \kirill\segnora\Models\card::find( Input::get('id') );

        $t = \kirill\segnora\Models\size::where('card_id', '=', $tovar->id)
        ->where('size', '=', Request::get('size'))->get()[0];
      
        $bsk= Db::table('kirill_segnora_basket');

        $bsk_tovar= Db::table('kirill_segnora_basket_tovar');

        $bsk_check = \kirill\segnora\Models\basket::where('id_basket','=',$user->id)->get();

        if(sizeof($bsk_check) == 0)
        {
          $bsk->insert([
            [
              'id_basket'=>$user->id,
              'created_at' => NOW(),
              'updated_at' => NOW()]
          ]);

          if($t->quantity>0)
          {
            Db::table('kirill_segnora_basket_tovar')->insert([
              ['id_card' =>  $tovar->id,
              'id_basket_tovar'=>$user->id,
              'size' => Request::get('size')]
            ]);
            $t->quantity -= 1;
            $t->save();
            return [
              '#tovarinfo' => $this->renderPartial('card_info.htm', ['record'=>$tovar,'user'=>Auth::getUser()]),
              'valid'=>'good',
            ];
          }
          else{
            return [
              '#tovarinfo' => $this->renderPartial('card_info.htm', ['record'=>$tovar,'user'=>Auth::getUser()]),
              'valid'=>'error',
            ];
          }
        }
        else
        {
          $bsk_check = $bsk_check[0];
          $bsk_check->updated_at = now();
          $bsk_check->save();
          if($t->quantity>0)
          {
            Db::table('kirill_segnora_basket_tovar')->insert([
              ['id_card' =>  $tovar->id,
              'id_basket_tovar'=>$user->id,
              'size' => Request::get('size')]
            ]);
            $t->quantity -= 1;
            $t->save();
            return [
              '#tovarinfo' => $this->renderPartial('card_info.htm', ['record'=>$tovar,'user'=>Auth::getUser()]),
              'valid'=>'good',
            ];
          }
          else{
            return [
              '#tovarinfo' => $this->renderPartial('card_info.htm', ['record'=>$tovar,'user'=>Auth::getUser()]),
              'valid'=>'error',
            ];
          }
        }
    }

    public function onGetOrderDetails(){
      $tovars = \kirill\segnora\Models\Order::where('id_user', '=', $user = Auth::getUser()->id)
      ->where('order_number', '=', Request::get('t'))->get()[0]->order_tovar;
      #Db::table('kirill_segnora_order')->
        #join('kirill_segnora_card', 'kirill_segnora_card.id', '=', 'kirill_segnora_order.id_card')->
        #where('order_number', '=', Request::get('t'))->get();
      $s=0;
      foreach ($tovars as $t){
        $s+=$t->oldprice;
      }
      return [
        '#order' => $this->renderPartial('history_info.htm', ['records2'=>$tovars, "sum"=>$s])
      ];
  }
    //ПЕРЕДЕЛАЛ
    public function onAddToOrder(){
      
      #Db::table('kirill_segnora_basket')->where('id_user', '=', $user = Auth::getUser()->id)->get();
      if(Request::get('city')!='' || Request::get('adress_magazin')!='')
      {
        $ubsk = \kirill\segnora\Models\Basket::where('id_basket', '=' , $user = Auth::getUser()->id);
        $time = time();
        $time1 = floor($time%1000);
        $time2 = floor(($time/1000)%1000);
        $time3 = floor(($time/1000000)%1000);
        $time4 = floor(($time/1000000000)%1000);
        $on = sprintf("%04d-%03d-%03d-%03d-%03d",Auth::getUser()->id,$time1,$time2,$time3,$time4);
        $id = Db::table('kirill_segnora_order')->insertGetId([
            'order_number'=> $on,
            'id_user' => $user = Auth::getUser()->id,
            'created_at' => NOW(),
            'email'=> Request::get('email'),
            'name'=> Request::get('name'),
            'surname'=>Request::get('surname'),
            'adress'=>Request::get('adress'), //правильно пишется address
            ((Request::get('when')=='dost')?'city':'adress_magazin')=>
            ((Request::get('when')=='dost')?Request::get('city'):Request::get('adress_magazin')),
            'phone'=>Request::get('phone'),
        ]);
          $tovars = $ubsk->get()[0]->basket_tovar;
        foreach($tovars as $i)
        {
          Db::table('kirill_segnora_order_tovar')->insert([
            [
              'order_id' => $id,
              'id_card' =>  $i->id_card,
              'size' => $i->size,
              'oldprice'=> ($i->card->discount)?$i->card->discount:$i->card->price],
          ]);
          $i->delete();
        }
        //$tovars->delete();
        $ubsk->delete();
        //$tovars->save();
        //$ubsk->save();
        // $del = Db::table('kirill_segnora_basket');
        // foreach($del as $i)
        // {
        //   Db::table('kirill_segnora_basket')->where('id_user', '=', $user = Auth::getUser()->id)->delete();
          
        // }
        
        return [
          '.bsk' => $this->renderPartial('bsk.htm'),
        ];
      }
      else {
        echo "Вы не выбрали способ получения заказа";
      }
  }
}
