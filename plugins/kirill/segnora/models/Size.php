<?php namespace Kirill\Segnora\Models;

use Model;

/**
 * Model
 */
class Size extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_size';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'card' => 'Kirill\segnora\Models\Card'
    ];
}
