<?php namespace Kirill\Segnora\Models;

use Model;

/**
 * Model
 */
class Order_tovar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_order_tovar';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo = [
        'card' => ['kirill\segnora\models\Card', 'key' => 'id_card'],
        
        'order' => ['kirill\segnora\models\order', 'key' => 'id', 'otherKey' => 'order_id'],
    ];
    
}
