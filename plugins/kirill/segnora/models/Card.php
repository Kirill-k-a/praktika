<?php namespace Kirill\Segnora\Models;

use Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

/**
 * Model
 */
class Card extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_card';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $hasMany = [
        'sizes' => ['kirill\segnora\Models\Size',
        'delete' => true],
    ];

    
    protected $fillable = ['Card','Size'];

    
    public $belongsTo = [
    'Country' => ['kirill\segnora\models\Country', 'key' => 'id_country'],
    'Type' => ['kirill\segnora\models\Type', 'key' => 'id_clothing_type']
    ];

    public function getIdCountryOptions($keyValue = null)
    {
        return Country::lists('name_country', 'id');
    }
    
    public function getIdClothingTypeOptions($keyValue = null)
    {
        return Type::lists('type_clothing', 'id');
    }

    public function scopeIdd($query)
    {
        $countrys = Input::get('country');
        $types = Input::get('type');
        $sizes = Input::get('size');
        $ids = Input::get('id');
        $prices = Input::get('price');
        $discounts = Input::get('discount');
        $sort = Input::get('sort');


        if ($sizes) {
            $query->join('kirill_segnora_size', 'kirill_segnora_card.id', '=', 'kirill_segnora_size.card_id')
            ->Select('kirill_segnora_card.*', 'kirill_segnora_size.size')->groupBy('kirill_segnora_card.id');
            $query->where(function ($query) {
                foreach(Input::get('size') as $selectsize) {
                    $query->orWhere('kirill_segnora_size.size', '=', $selectsize);
                }
            });
        }



        if ($ids){
            $query->where(function ($query) {
                foreach(Input::get('id') as $id) {
                    $query->orWhere('kirill_segnora_card.id', $id);
                }
            });
        }

        if ($types){
            $query->where(function ($query) {
                foreach($types = Input::get('type') as $type) {
                    $query->orWhere('kirill_segnora_card.id_clothing_type', $type);
                }
            });
        }


        if ($countrys){
            $query->where(function ($query) {
                foreach( $countrys = Input::get('country') as $country) {
                    $query->orWhere('kirill_segnora_card.id_country', $country);
                }
            });
        }
        
        if ($sort == 'desc')
            $query->orderBy('kirill_segnora_card.id','desc');
        if ($sort == 'asc')
            $query->orderBy('kirill_segnora_card.id','asc');

        if ($prices == 'desc')
            //$query->orderBy('price','desc');
            $query->Select('kirill_segnora_card.*',DB::raw('CASE WHEN discount>0 THEN discount  ELSE price end as realprice'))->orderBy('realprice','desc');
            
        if ($prices == 'asc')
            //$query->orderBy('price','asc');
            $query->Select('kirill_segnora_card.*',DB::raw('CASE WHEN discount>0 THEN discount  ELSE price end as realprice'))->orderBy('realprice','asc');

        if ($discounts)
            $query->Where('kirill_segnora_card.discount', '>', '0');

        return $query;
    }
}
