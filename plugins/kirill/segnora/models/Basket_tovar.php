<?php namespace Kirill\Segnora\Models;

use Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
/**
 * Model
 */
class Basket_tovar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_basket_tovar';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $belongsTo = [
        'card' => ['kirill\segnora\models\Card', 'key' => 'id_card'],
        // Default: basket_tovar.basket_id => Basket.id
        //          basket_tovar.<Key> => Basket.<otherkey>
        'basket' => ['kirill\segnora\models\Basket', 'key' => 'id_basket_tovar', 'otherKey' => 'id_basket'],
    ];
    public function scopeBasket2($cardSort)
    {
        $userid = Auth::getUser()->id;
        $cardSort->Where('id_basket_tovar','=', $userid);
        return $cardSort;
    }
}
