<?php namespace Kirill\Segnora\Models;

use Model;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;

//use Kirill\Segnora\Components\basket;

/**
 * Model
 */
class Basket extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_basket';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    
    public $belongsTo = [
        'user' => ['RainLab\User\Models\User', 'key' => 'id_basket'],
        // 'card' => ['kirill\segnora\models\Card', 'key' => 'id_card'],
        
    ];
    public $hasMany = [
        'basket_tovar' => ['kirill\segnora\Models\Basket_tovar',
        'key' => 'id_basket_tovar',
        'otherKey' => 'id_basket',
        'delete' => true],
    ];
    
}
