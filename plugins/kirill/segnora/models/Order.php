<?php namespace Kirill\Segnora\Models;

use Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Auth;
/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kirill_segnora_order';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    

    public $hasMany = [
        'order_tovar' => 'kirill\segnora\Models\order_tovar',
    ];

    public function scopeOrder($cardSort)
    {
        $userid = Auth::getUser()->id;
        $cardSort->Where('kirill_segnora_order.id_user','=', $userid)
        ->groupBy('order_number');
    }
}
