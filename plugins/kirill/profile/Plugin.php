<?php namespace Kirill\Profile;

use System\Classes\PluginBase;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Models\User as UserModel;
use Event;
use Session;

class Plugin extends PluginBase
{
    public function registerComponents()
    {

    }

    public function registerSettings()
    {
        
    }

    public function boot(){

        UserModel::extend(function ($model){
            $model->addFillable([
                'phone',
                'adress'
            ]);
        });


        UsersController::extendFormFields(function($form, $model, $context){
            $form->addTabFields([
                'phone'=>[
                    'label'=>'Телефон',
                    'type'=>'text',
                    'tab'=>'profile'
                ],
                'adress'=>[
                    'label'=>'Адресс',
                    'type'=>'text',
                    'tab'=>'profile'
                ]
            ]);
        });

    }
}
