<?php namespace kirill\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields2 extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('vk','emaill');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('emaill', 'vk');
        });
    }

}
