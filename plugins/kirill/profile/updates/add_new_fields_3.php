<?php namespace kirill\Profile\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields3 extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('emaill','vk');
        });
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('vk', 'emaill');
        });
    }

}
