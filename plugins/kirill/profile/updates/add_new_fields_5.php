<?php namespace kirill\Profile\Updates; 

use Schema; 
use October\Rain\Database\Schema\Blueprint; 
use October\Rain\Database\Updates\Migration; 

class AddNewFields5 extends Migration 
{ 
    public function up() 
    { 
        Schema::table('users', function($table) { 
        $table->string('session')->nullable(); 
        }); 
    } 

    public function down()
    { 
        Schema::table('users', function($table) { 
        $table->dropColumn('session'); 
        }); 
    } 
}