<?php namespace kirill\Profile\Updates; 

use Schema; 
use October\Rain\Database\Schema\Blueprint; 
use October\Rain\Database\Updates\Migration; 

class AddNewFields4 extends Migration 
{ 
    public function up() 
    { 
        Schema::table('users', function($table) { 
        $table->string('adress')->nullable();
        $table->string('phone')->nullable();
        $table->dropColumn('vk'); 

        }); 
    } 

    public function down()
    { 
        Schema::table('users', function($table) { 
        $table->dropColumn('adress');
        $table->dropColumn('phone');
        $table->string('vk')->nullable(); 
        }); 
    } 
}