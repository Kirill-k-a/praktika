<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRainlabUserMailBlockers extends Migration
{
    public function up()
    {
        Schema::table('rainlab_user_mail_blockers', function($table)
        {
            $table->string('adress');
        });
    }
    
    public function down()
    {
        Schema::table('rainlab_user_mail_blockers', function($table)
        {
            $table->dropColumn('adress');
        });
    }
}
