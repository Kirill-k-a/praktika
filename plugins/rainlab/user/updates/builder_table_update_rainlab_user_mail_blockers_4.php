<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateRainlabUserMailBlockers4 extends Migration
{
    public function up()
    {
        Schema::table('rainlab_user_mail_blockers', function($table)
        {
            $table->dropColumn('adress');
        });
    }
    
    public function down()
    {
        Schema::table('rainlab_user_mail_blockers', function($table)
        {
            $table->string('adress', 191)->nullable();
        });
    }
}
