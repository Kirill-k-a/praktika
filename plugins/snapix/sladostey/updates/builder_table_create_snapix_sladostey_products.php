<?php namespace Snapix\Sladostey\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixSladosteyProducts extends Migration
{
    public function up()
    {
        Schema::create('snapix_sladostey_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->text('full_description')->nullable();
            $table->smallInteger('sort')->default(0);
            $table->smallInteger('qty')->unsigned();
            $table->integer('price')->unsigned();
            $table->integer('action_price')->nullable()->unsigned();
            $table->smallInteger('weight')->unsigned();
            $table->smallInteger('rating')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_sladostey_products');
    }
}
