<?php namespace Snapix\Sladostey\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixSladosteyProductsToCategories extends Migration
{
    public function up()
    {
        Schema::create('snapix_sladostey_products_to_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('product_id')->unsigned();
            $table->smallInteger('category_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_sladostey_products_to_categories');
    }
}
