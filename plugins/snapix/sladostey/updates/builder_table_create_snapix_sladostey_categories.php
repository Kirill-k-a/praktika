<?php namespace Snapix\Sladostey\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixSladosteyCategories extends Migration
{
    public function up()
    {
        Schema::create('snapix_sladostey_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->smallInteger('sort')->default(0);
            $table->smallInteger('parent_id')->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_sladostey_categories');
    }
}
