<?php namespace Snapix\Sladostey\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSnapixSladosteyOrders extends Migration
{
    public function up()
    {
        Schema::create('snapix_sladostey_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('client');
            $table->string('phone');
            $table->string('address');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->dateTime('delivery_date');
            $table->text('contents');
            $table->smallInteger('status_id')->unsigned()->default(0);
            $table->text('comment')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('snapix_sladostey_orders');
    }
}
