<?php namespace Snapix\Sladostey\Models;

use Model;
use System\Models\File;

/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'product' => Product::class,
    ];

    public $hasOne = [
        'category' => [ Category::class, 'key' => 'parent_id', 'otherKey' => 'id' ],
    ];

    public $belongsToMany = [
        'products' => [ Product::class,
            'table' => 'snapix_sladostey_products_to_categories',
            'key' => 'category_id',
            'otherKey' => 'product_id' ]
    ];

    public $attachOne = [
        'image' => File::class
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'snapix_sladostey_categories';
}
